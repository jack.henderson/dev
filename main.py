###############################################################################
# Entry
###############################################################################

import sys, os, codecs, traceback, time
from utils import out, fatal, devfile
from utils.err import DevException
import core

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

def assert_initialised():
    if not os.path.exists(os.path.join(os.getcwd(), '.dev')):
        fatal('Development environment not initialised! (Try `dev init`)')


def pre_check():
    devfile.assert_exists()

CORE_COMMANDS: dict = {
    '__precheck__': {
        'do': pre_check
    },
    'clean': {
        'help': 'Removes all files and directories generated by DEV.',
        'do': core.Clean
    },
    'init': {
        'help': 'Initialise the development environment.',
        'do': core.Init,
    },
    'install': {
        'help': 'Install all requirements.',
        'do': core.Install,
    },
    'start': {
        'do': core.Start,
    }
}

def print_help():
    out.inf('Usage:')
    for cmd_name, cmd in CORE_COMMANDS.items():
        if 'help' in cmd:
            out.inf(' %s' % cmd_name)
            out.inf('   %s' % cmd['help'])

def execute_command(cmd):
    try:
        inst = cmd()

        if not inst:
            return

        if inst.require_init():
            assert_initialised()

        inst()

    except DevException:
        exit(1)
    except:
        traceback.print_exc()
        out.err(u'Crashed :(')
        exit(1)

if __name__ == '__main__':
    args = sys.argv[1:]

    try:
        devfile.assert_exists()
    except DevException:
        exit(1)

    num_args = len(args)
    command = args[0]

    if command in CORE_COMMANDS:
        execute_command(CORE_COMMANDS[command]['do'])
    else:
        out.err('What is \'%s\'?' % command)
        print_help()
        exit(1)

    exit(0)
