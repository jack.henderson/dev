import os, sys, imp, traceback
from . import out, fatal

def assert_exists() -> str:
    sys.path.append(os.getcwd())
    path = os.path.join(os.getcwd(), 'dev')
    
    if not os.path.exists(path) \
        or os.path.isdir(path):
        fatal('No dev file at %s/!' % os.getcwd())

    return path


def load(ctx: dict={}) -> object:

    ###############################################################################
    # Load dev file
    ###############################################################################

    path = assert_exists()
    
    try:
        devfile = imp.load_source('devfile', path)
    except Exception as ex:
        fatal('Error reading dev file.', ex)


    ###############################################################################
    # Set default values
    ###############################################################################
    
    def default(k, v):
        nonlocal devfile
        if not k in devfile.__dict__:
            devfile.__dict__[k] = v

    def do_nothing(): pass

    default('NAME', os.path.split(path)[-1])
    default('VERSION', (0, 0, 0))
    default('init', do_nothing)
    default('start', do_nothing)

    devfile._VERSION_STR = '%s.%s.%s' % devfile.VERSION

    ###############################################################################
    # Inject context
    ###############################################################################
    
    def inject(k, v):
        nonlocal devfile
        if k in devfile.__dict__:
            fatal('Reserved term \'%s\' re-declared in dev file as %s' % (
                k, devfile.__dict__[k]
            ))

        devfile.__dict__[k] = v

    for k, v in ctx.items():
        inject(k, v)

    return devfile
