import abc
from . import out

class Command(metaclass=abc.ABCMeta):
    def __init__(self):
        self._warnings = []

    @abc.abstractmethod
    def do(self): pass

    def warn(self, msg : str) -> None:
        self._warnings.append(msg)

    def get_finish_message(self) -> str:
        return 'Finished'

    def require_init(self) -> bool:
        return True

    def __call__(self):
        self.do()
        if self._warnings:
            out.war('%s, with warnings...' % self.get_finish_message())
            for warning in self._warnings:
                out.war(warning)
        else:
            out.win('%s!' % self.get_finish_message())