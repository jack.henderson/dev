###############################################################################
# Output & logging.
###############################################################################

import os
import sys

def _write(msg: str, icon: str, format: str = '') -> None:
    ''' Write to stdout. '''
    
    sys.stdout.write(format)
    sys.stdout.write('\033[1m')

    msg = u' %s %s' % (icon, msg)

    cols = int(os.popen('stty size', 'r').read().split()[1])
    padding = cols - len(msg)
    sys.stdout.write(u'%s%s\033[1;0;0m\n' % (msg, u' '*padding))
    sys.stdout.flush()


def inf(msg: str) -> None:
    ''' Log some info '''
    _write(msg, u'❯', '\033[7m')

def war(msg: str) -> None:
    ''' Log a warning '''
    _write(msg, u'‼', '\033[1;30;43m')

def err(msg: str) -> None:
    ''' Log an error '''
    _write(msg, u'✘', '\033[1;33;41m')

def win(msg: str) -> None:
    ''' Log success '''
    _write(msg, u'✔', '\033[1;37;44m')

def ex(msg: str, ex: Exception) -> None:
    err(str(ex))
    err(msg)
    sys.stdout.flush()