import sys, traceback
from . import out

def fatal(msg: str, ex: Exception=None) -> None:
    if ex:
        out.err('Fatal error:')
        sys.stdout.flush()
        traceback.print_exc()

    out.err(msg)

    raise DevException()

class DevException(Exception):
    def __init__(self,):
        super().__init__()