from . import native, pip

installers = [
    native.native_install,
    pip.pip_install,
]

def install(devfile, warn):
    count = 0
    for installer in installers:
        count += installer(devfile, warn)
    return count