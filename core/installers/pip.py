import os
from utils import out

def pip_install(devfile, warn):
    if not 'pip' in devfile.__dict__ or not devfile.pip:
        return 0

    out.inf('Installing pip requirements...')

    count = 0
    for req in devfile.pip:
        is_file = False
        if os.path.exists(req) or os.path.exists(os.path.join(os.getcwd(), req)):
            is_file = True

        if os.system('.dev/bin/pip install %s %s' % (
            '-r' if is_file else '', req)):
            warn('Failed to pip install: %s.' % req)
        else:
            count += 1

    return count