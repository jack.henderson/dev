import os
from utils import out

def native_install(devfile, warn):
    if not 'native' in devfile.__dict__ or not devfile.native:
        return 0

    out.inf('Installing native requirements...')

    count = 0
    for req in devfile.native:
        if os.system('wget -q --show-progress %s -P .dev/native' % req):
            warn('Error downloading native requirement: %s.' % req)
        else:
            count += 1

    return count
