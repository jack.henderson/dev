from utils import command, devfile
from . import installers

class Install(command.Command):

    def do(self):
        self.install_count = installers.install(devfile.load(), self.warn)

    def get_finish_message(self):
        if self.install_count:
            return 'Installed %d packages & libraries' % self.install_count
        return 'Nothing to install'
