import os, venv
from utils import out, devfile, command
from . import installers

class Init(command.Command):    
    def do(self):

        ###############################################################################
        # Load dev file
        ###############################################################################

        dev = devfile.load()

        out.inf('Initialising %s %s' % (
            dev.NAME, dev._VERSION_STR))

        dev.init()

        ###############################################################################
        # Virtual environment
        ###############################################################################
        
        out.inf('Creating virtual environment...')
        venv_path = os.path.join(os.getcwd(), '.dev')
        venv.create(
            venv_path,
            clear=True,
            symlinks=True,
            with_pip=True)

        lib_env = os.environ.get('LD_LIBRARY_PATH', '')
        lib_path = os.path.join(venv_path, 'native')
        lib_env = '%s:%s' % (lib_env, lib_path)

        activation_path = os.path.join(venv_path, 'bin/activate')
        
        os.system('echo "export LD_LIBRARY_PATH=%s" >> %s' % (
            lib_env, activation_path))

        ###############################################################################
        # Install
        ###############################################################################
        
        installers.install(devfile, self.warn)

    def get_finish_message(self):
        return 'Initialised'

    def require_init(self):
        return False
