import os
from utils import devfile, command, out

class Start(command.Command):

    def do(self):
        dev_file = devfile.load()
        dev_file.start()

    def get_finish_message(self):
        return 'Started'
