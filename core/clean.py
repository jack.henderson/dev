import shutil
from utils import out, command

class Clean(command.Command):
    def do(self):
        try:
            shutil.rmtree('./.dev')
        except FileNotFoundError:
            self.warn('Nothing to clean!')

    def get_finish_message(self):
        return 'Cleaned'