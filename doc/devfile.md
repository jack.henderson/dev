# Dev file

The 'dev file' is a file simply called `dev`, which resides in the root of the
project. It is a python script which describes the process of setting up the
development environment.

----
# Properties

| property | type | description |
|--:|:-:|:--|
| NAME      | `str`             | Name of the project.<br>**Default:** name of the directory containing the dev file. |
| VERSION   | `(int, int, int)` | Tuple containing the (MAJOR, MINOR, PATCH) versions.<br>**Default:** `(0, 0, 0)`

# Functions

## init()
Here the development environment is initialised.

| function | args | return | description |
|--:|:-:|:-:|:--|
| pip() | `*str` | `None` | Declare any number of pip packages or requirements files.
| native() | `*str` | `None` | Declare any number of urls to download native libraries required.